<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Weltladen_Wasserburg_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="news-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h3 class="entry-title">', '</h3>' );
		else :
			the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
            <div class="news-details">
                <span><i class="fa fa-clock-o"></i> <time><?php the_date();?></time></span>
                <span><i class="fa fa-user"></i> <?php the_author();?></span>
            </div>
			<?php
		endif; ?>
    </header><!-- .entry-header -->
    <div class="row newsteaser">
        <div class="col-md-4 newsteaser-image-col">
            <?php the_post_thumbnail('medium');?>
        </div>
        <div class="col-md-8">
            <?php is_singular() ? the_content() : the_excerpt();?>
        </div>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
