<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Weltladen_Wasserburg_Theme
 */

get_header(); ?>

<div class="container news-single-container">
    <div class="row" id="primary">
        <main id="content" class="col-sm-12" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content-single', get_post_type() );

			endwhile; // End of the loop.
			?>
            <p class="news-back-link"><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) )?>">zur Beitragsübersicht</a></p>
        </main><!-- #main -->
    </div>
</div><!-- #primary -->

<?php get_footer( 'custom' ); ?>
