<?php
/**
 * Template Name: Verein
 */
// Advanced Custom Fields
// Aufmacher
$aufmacher_bild = get_field( 'aufmacher_bild' );
$aufmacher_text = get_field( 'aufmacher_text');
// INITIATIVE
$initiative_icon         = get_field( 'initiative_icon' );
$initiative_ueberschrift = get_field( 'initiative_ueberschrift' );
$initiative_text         = get_field( 'initiative_text' );
// VORSTAND
$vorstand_icon         = get_field( 'vorstand_icon' );
$vorstand_ueberschrift = get_field( 'vorstand_ueberschrift' );
$vorstand_untertitel   = get_field( 'vorstand_untertitel' );
$vorstand_bild         = get_field( 'vorstand_bild' );
$vorstand_text         = get_field( 'vorstand_text' );
$vorstand_satzung_icon = get_field( 'vorstand_satzung_icon' );
$vorstand_satzung      = get_field( 'vorstand_satzung' );
$vorstand_satzung_text = get_field( 'vorstand_satzung_text' );
// PARALLAX
$parallax_bild = get_field( 'parallax_bild' );
// PROJEKTE
$projekte_icon         = get_field( 'projekte_icon' );
$projekte_ueberschrift = get_field( 'projekte_ueberschrift' );
$projekte_link_beschreibung    = get_field( 'projekte_link_beschreibung' );

get_header(); ?>

<!-- PARALLAX -->
<section id="aufmacher" class="parallax-section" data-type="background" data-speed="5"
	<?php if ( ! empty( $aufmacher_bild ) ) : ?>
        style="background: url('<?php echo $aufmacher_bild['url'] ?>') 50% 0 no-repeat;"
	<?php endif; ?>
>
    <p class="aufmacher-text"><?php echo $aufmacher_text ?></p>
</section>
<!-- INITIATIVE -->
<section id="initiative">
    <div class="container">
        <h2><i class="fa <?php echo $initiative_icon ?>"></i>&nbsp;<?php echo $initiative_ueberschrift ?></h2>
        <div class="row">
            <div class="col-sm-12">
				<?php echo $initiative_text ?>
            </div>
        </div>
    </div>
</section>

<!-- VORSTAND -->
<section id="vorstand" class="figure-section">
    <div class="container">
        <h2><i class="fa <?php echo $vorstand_icon ?>"></i>&nbsp;<?php echo $vorstand_ueberschrift ?></h2>
        <p class="subtitle"><?php echo $vorstand_untertitel ?></p>
        <div class="row">
            <div class="col-sm-12 figure-section-image-container">
                <figure class="figure-section-image-figure">
                    <img src="<?php echo $vorstand_bild['url'] ?>"alt="<?php echo $vorstand_bild['alt'] ?>">
                    <figcaption class="figure-section-image-figure-caption">
						<?php echo $vorstand_text ?>
                    </figcaption>
                </figure>
                <p class="figure-section-satzung" >
                    <i class="fa <?php echo $vorstand_satzung_icon ?>"></i>
                    &nbsp;
                    <a href="<?php echo $vorstand_satzung ?>" target="_blank"><?php echo $vorstand_satzung_text ?></a>
                </p>
            </div>
        </div>
    </div>
</section>

<!-- PARALLAX -->
<section id="parallax" class="parallax-section parallax-section-small" data-type="background" data-speed="0"
         style="background: url('<?php echo $parallax_bild['url'] ?>') 50% 0 no-repeat fixed;">
</section>

<!-- PROJEKTE -->
<section id="projekte" class="projekte-section">
    <div class="container">
        <h2><i class="fa <?php echo $projekte_icon ?>"></i>&nbsp;<?php echo $projekte_ueberschrift ?></h2>
		<?php
		$args        = array(
			'posts_per_page'   => '10',
			'offset'           => 0,
			'category'         => '',
			'category_name'    => '',
			'orderby'          => 'date',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => '',
			'meta_value'       => '',
			'post_type'        => 'project',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'           => '',
			'author_name'      => '',
			'post_status'      => 'publish',
			'suppress_filters' => false
		);
		$posts_array = get_posts( $args );
		foreach ( $posts_array as $post ) {
			$title       = get_the_title();
			$description = get_post_meta( get_the_ID(), 'projekt_beschreibung', true );
			$link        = get_post_meta( get_the_ID(), 'projekt_link', true );
			echo '<div class="row">
                    <div class="col-sm-12">
                        <h3>'.$title.'</h3>
                        <p>
                            '.$description.'
                        </p>
                        <p>'.$projekte_link_beschreibung.' <a href="' . $link . '" target="_blank">'.$link.'</a></p>
                    </div>
                </div>';
		}
		?>
    </div>
</section>

<?php get_footer( 'custom' ); ?>
