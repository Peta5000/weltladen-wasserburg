<?php
/**
 * Template Name: Einfache Seite
 */
// Advanced Custom Fields
// Aufmacher
$aufmacher_bild = get_field( 'aufmacher_bild' );
$aufmacher_text = get_field( 'aufmacher_text');
// Simple section
$section_content   = get_field('section_content');

get_header(); ?>

<!-- PARALLAX -->
<section id="aufmacher" class="parallax-section" data-type="background" data-speed="5"
	<?php if ( ! empty( $aufmacher_bild ) ) : ?>
        style="background: url('<?php echo $aufmacher_bild['url'] ?>') 50% 0 no-repeat;"
	<?php endif; ?>
>
    <p class="aufmacher-text"><?php echo $aufmacher_text ?></p>
</section>
<!-- SIMPLE SECTION -->
<section id="simple" class="simple-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 simple-section-content">
	            <?php echo $section_content ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer( 'custom' ); ?>
