<?php
/**
 * Template Name: Laden
 */
// Advanced Custom Fields
// Aufmacher
$aufmacher_bild = get_field( 'aufmacher_bild' );
$aufmacher_text = get_field( 'aufmacher_text');
// LADEN
$laden_icon                = get_field( 'laden_icon' );
$laden_ueberschrift        = get_field( 'laden_ueberschrift' );
$laden_google_map          = get_field( 'laden_google_map' );
$laden_adresse             = get_field( 'laden_adresse' );
$laden_zeiten_ueberschrift = get_field( 'laden_zeiten_ueberschrift' );
$laden_zeiten              = get_field( 'laden_zeiten' );
// PRODUKTE
$produkte_icon                 = get_field( 'produkte_icon' );
$produkte_ueberschrift         = get_field( 'produkte_ueberschrift' );
$produkte_untertitel           = get_field( 'produkte_untertitel' );
$produkte_partner_ueberschrift = get_field( 'produkte_partner_ueberschrift' );
$produkte_partner              = get_field( 'produkte_partner' );
// PARALLAX
$parallax_bild = get_field( 'parallax_bild' );
// INITIATIVE
$idee_icon                      = get_field( 'idee_icon' );
$idee_ueberschrift              = get_field( 'idee_ueberschrift' );
$idee_text                      = get_field( 'idee_text' );
$idee_bild                      = get_field( 'idee_bild' );
$idee_info_ueberschrift         = get_field( 'idee_info_ueberschrift' );
$idee_konvention_ueberschrift   = get_field( 'idee_konvention_ueberschrift' );
$idee_konvention                = get_field( 'idee_konvention' );
$idee_handel_ueberschrift       = get_field( 'idee_handel_ueberschrift' );
$idee_handel                    = get_field( 'idee_handel' );
// mitarbeiter
$mitarbeiter_icon         = get_field( 'mitarbeiter_icon' );
$mitarbeiter_ueberschrift = get_field( 'mitarbeiter_ueberschrift' );
$mitarbeiter_untertitel   = get_field( 'mitarbeiter_untertitel' );
$mitarbeiter_bild         = get_field( 'mitarbeiter_bild' );
$mitarbeiter_text         = get_field( 'mitarbeiter_text' );

get_header(); ?>

<!-- PARALLAX -->
<section id="aufmacher" class="parallax-section" data-type="background" data-speed="5"
	<?php if ( ! empty( $aufmacher_bild ) ) : ?>
        style="background: url('<?php echo $aufmacher_bild['url'] ?>') 50% 0 no-repeat;"
	<?php endif; ?>
>
    <p class="aufmacher-text"><?php echo $aufmacher_text ?></p>
</section>
<!-- LADEN -->
<section id="laden" class="laden-section">
    <div class="container">
        <h2><i class="fa <?php echo $laden_icon ?>"></i>&nbsp;<?php echo $laden_ueberschrift ?></h2>
        <div class="row">
        <!--
            <div class="col-sm-6">
                <iframe src="<?php echo $laden_google_map ?>" width="600" height="450" frameborder="0" style="border:0"
                        allowfullscreen></iframe>
            </div>
        -->
            <div class="col-sm-12 laden-text-container">
				<?php echo $laden_adresse ?>
                <h3><?php echo $laden_zeiten_ueberschrift ?></h3>
				<?php echo $laden_zeiten ?>
            </div>
        </div>
    </div>
</section>

<!-- PRODUKTE -->
<?php
$args     = array(
	'posts_per_page'   => '20',
	'offset'           => 0,
	'category'         => '',
	'category_name'    => '',
	'orderby'          => 'date',
	'order'            => 'DESC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'product',
	'post_mime_type'   => '',
	'post_parent'      => '',
	'author'           => '',
	'author_name'      => '',
	'post_status'      => 'publish',
	'suppress_filters' => false
);
$products = get_posts( $args );
?>
<section id="produkte" class="produkte-section">
    <div class="container">
        <h2><i class="fa <?php echo $produkte_icon ?>"></i>&nbsp;<?php echo $produkte_ueberschrift ?></h2>
        <p class="subtitle"><?php echo $produkte_untertitel ?></p>
        <div class="row">
            <div class="col-sm-12">
                <div id="produkte-carousel" class="carousel slide" data-ride="carousel" data-interval="3000">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
						<?php
						foreach ( $products as $key => $post ) {
						    if ($key == 0) {
							    echo '<li data-target="#produkte-carousel" data-slide-to="' . $key . '" class="active"></li>';
                            } else {
							    echo '<li data-target = "#produkte-carousel" data-slide-to = "' . $key . '" ></li >';
                            }
						}
						?>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
						<?php
						foreach ( $products as $key => $post ) {
							$title      = get_the_title();
							$bild       = wp_get_attachment_image(get_post_meta( get_the_ID(), 'field_product_bild', true ), 'full');
						    $active = '';
						    if ($key == 0) {
						        $active = 'active';
                            } else {
						        $active = '';
                            }
							echo '<div class="item '.$active.'">
                                        ' . $bild . '
                                        <div class="carousel-caption">
                                            ' . $title . '
                                        </div>
                                  </div>';
						}
						?>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#produkte-carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#produkte-carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel-group" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default more-panel partner-panel">
                        <div class="panel-heading" role="tab" id="headingOnePartner">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseOnePartner"
                                   aria-expanded="true" aria-controls="collapseOnePartner">
									<?php echo $produkte_partner_ueberschrift ?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOnePartner" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingOnePartner">
                            <div class="panel-body">
								<?php echo $produkte_partner ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- PARALLAX -->
<section id="parallax" class="parallax-section parallax-section-small" data-type="background" data-speed="0"
         style="background: url('<?php echo $parallax_bild['url'] ?>') 50% 0 no-repeat fixed;">
</section>

<!-- IDEE -->
<section id="idee">
    <div class="container">
        <h2><i class="fa <?php echo $idee_icon ?>"></i>&nbsp;<?php echo $idee_ueberschrift ?></h2>
        <div class="row">
            <div class="col-sm-6">
                <img src="<?php echo $idee_bild['url'] ?>" alt="<?php echo $idee_bild['alt'] ?>">
            </div>
            <div class="col-sm-6">
	            <?php echo $idee_text ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h3><?php echo $idee_info_ueberschrift ?></h3>
            </div>
            <div class="col-sm-12">
                <div class="panel-group" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default more-panel">
                        <div class="panel-heading" role="tab" id="headingOneIdee">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOneIdee"
                                   aria-expanded="true" aria-controls="collapseOneIdee">
	                                <?php echo $idee_konvention_ueberschrift ?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOneIdee" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingOneIdee">
                            <div class="panel-body">
	                            <?php echo $idee_konvention ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default more-panel">
                        <div class="panel-heading" role="tab" id="headingTwoIdee">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseTwoIdee" aria-expanded="false" aria-controls="collapseTwoIdee">
	                                <?php echo $idee_handel_ueberschrift ?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwoIdee" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingTwoIdee">
                            <div class="panel-body">
	                            <?php echo $idee_handel ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- MITARBEITER -->
<section id="mitarbeiter" class="figure-section">
    <div class="container">
        <h2><i class="fa <?php echo $mitarbeiter_icon ?>"></i>&nbsp;<?php echo $mitarbeiter_ueberschrift ?></h2>
        <p class="subtitle"><?php echo $mitarbeiter_untertitel ?></p>
        <div class="row">
            <div class="col-sm-12 figure-section-image-container">
                <figure class="figure-section-image-figure">
                    <img src="<?php echo $mitarbeiter_bild['url'] ?>" alt="<?php echo $mitarbeiter_bild['alt'] ?>">
                    <figcaption class="figure-section-image-figure-caption">
	                    <?php echo $mitarbeiter_text ?>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>
</section>

<?php get_footer( 'custom' ); ?>
