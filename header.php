<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Weltladen_Wasserburg_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text"
       href="#content"><?php esc_html_e( 'Skip to content', 'weltladen-wasserburg' ); ?></a>

    <!-- HEADER -->
    <header class="site-header" role="banner">

        <!-- NAVBAR -->
        <div class="navbar-wrapper">
            <div class="navbar navbar-fixed-top" role="navigation" style="background-color:white;">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a href="/">
                            <img class="mobile-logo" alt="Brand"
                                 src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/logo.png" height="40">
                        </a>
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                    </div><!-- navbar-header -->

					<?php
					wp_nav_menu( array(

						'theme_location'  => 'menu-1',
						'container'       => 'nav',
						'container_class' => 'navbar-collapse collapse',
						'menu_class'      => 'nav navbar-nav'

					) );
					?>

                </div><!-- container-->
            </div><!-- navbar -->
        </div><!-- navbar-wrapper -->

    </header>
