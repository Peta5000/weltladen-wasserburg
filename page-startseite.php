<?php
/**
 * Template Name: Startseite
 */
// Advanced Custom Fields
// Aufmacher
$aufmacher_bild = get_field( 'aufmacher_bild' );
$aufmacher_text = get_field( 'aufmacher_text');
// Teaser
$teaser_icon                = get_field( 'teaser_icon' );
$teaser_ueberschrift        = get_field( 'teaser_ueberschrift' );
$teaser_1_bild              = get_field( 'teaser_1_bild' );
$teaser_1_text              = get_field( 'teaser_1_text' );
$teaser_1_link              = get_field( 'teaser_1_link' );
$teaser_1_ueberschrift      = get_field( 'teaser_1_ueberschrift' );
$teaser_2_bild              = get_field( 'teaser_2_bild' );
$teaser_2_text              = get_field( 'teaser_2_text' );
$teaser_2_link              = get_field( 'teaser_2_link' );
$teaser_2_ueberschrift      = get_field( 'teaser_2_ueberschrift' );
$teaser_3_bild              = get_field( 'teaser_3_bild' );
$teaser_3_text              = get_field( 'teaser_3_text' );
$teaser_3_link              = get_field( 'teaser_3_link' );
$teaser_3_ueberschrift      = get_field( 'teaser_3_ueberschrift' );
$teaser_button_text         = get_field( 'teaser_button_text' );
// Parallax
$parallax_bild = get_field( 'parallax_bild' );
// Newsteaser
$newsteaser_icon            = get_field( 'newsteaser_icon' );
$newsteaser_ueberschrift    = get_field( 'newsteaser_ueberschrift' );

get_header(); ?>

<!-- AUFMACHER -->
<section id="aufmacher" class="parallax-section home" data-type="background" data-speed="5"
	<?php if ( ! empty( $aufmacher_bild ) ) : ?>
        style="background: url('<?php echo $aufmacher_bild['url'] ?>') 50% 0 no-repeat;"
	<?php endif; ?>
>
    <p class="aufmacher-text"><?php echo $aufmacher_text ?></p>
</section>

<!-- TEASER -->
<section id="teaser" class="teaser-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="section-headline"><i
                            class="fa <?php echo $teaser_icon ?>"></i>&nbsp;<?php echo $teaser_ueberschrift ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <div class="thumbnail teaser-item">
                    <img src="<?php echo $teaser_1_bild['url'] ?>" alt="<?php echo $teaser_1_bild['alt'] ?>">
                    <div class="caption">
                        <h3><?php echo $teaser_1_ueberschrift ?></h3>
                        <p>
							<?php echo $teaser_1_text ?>
                        </p>
                        <p><a href="<?php echo $teaser_1_link ?>" class="btn btn-warning teaser-button"
                              role="button"><?php echo $teaser_button_text ?></a></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="thumbnail teaser-item">
                    <img src="<?php echo $teaser_2_bild['url'] ?>" alt="<?php echo $teaser_2_bild['alt'] ?>">
                    <div class="caption">
                        <h3><?php echo $teaser_2_ueberschrift ?></h3>
                        <p>
							<?php echo $teaser_2_text ?>
                        </p>
                        <p><a href="<?php echo $teaser_2_link ?>" class="btn btn-warning teaser-button"
                              role="button"><?php echo $teaser_button_text ?></a></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="thumbnail teaser-item">
                    <img src="<?php echo $teaser_3_bild['url'] ?>" alt="<?php echo $teaser_3_bild['alt'] ?>">
                    <div class="caption">
                        <h3><?php echo $teaser_3_ueberschrift ?></h3>
                        <p>
							<?php echo $teaser_3_text ?>
                        </p>
                        <p><a href="<?php echo $teaser_3_link ?>" class="btn btn-warning teaser-button"
                              role="button"><?php echo $teaser_button_text ?></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- PARALLAX -->
<section id="parallax" class="parallax-section parallax-section-small" data-type="background" data-speed="0"
         style="background: url('<?php echo $parallax_bild['url'] ?>') 50% 0 no-repeat fixed;">
</section>

<!-- NEWSTEASER -->
<section id="newsteaser" class="newsteaser-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="section-headline"><i class="fa <?php echo $newsteaser_icon ?>"></i>&nbsp;<?php echo $newsteaser_ueberschrift ?></h2>
            </div>
        </div>
		<?php
		$args        = array(
			'posts_per_page'   => 3,
			'offset'           => 0,
			'category'         => '',
			'category_name'    => '',
			'orderby'          => 'date',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => '',
			'meta_value'       => '',
			'post_type'        => 'post',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'           => '',
			'author_name'      => '',
			'post_status'      => 'publish',
			'suppress_filters' => false
		);
		$posts_array = get_posts( $args );
		foreach ( $posts_array as $post ) {
			$title     = get_the_title();
			$date      = get_the_date();
			$thumbnail = get_the_post_thumbnail();
			$post_link = "'" . get_permalink() . "'";

			$content = wp_strip_all_tags( get_extended( get_post_field( 'post_content' ) )['main'] );
			$content = strlen( $content ) > 150 ? substr( $content, 0, 150 ) . "..." : $content;

			echo '<div class="row newsteaser">
                    <div class="col-md-4 newsteaser-image-col">
                        ' . $thumbnail . '
                    </div>
                    <div class="col-md-8">
                        <h3 class="newsteaser-headline">' . $title . '</h3><span><strong>' . $date . '</strong></span>
                        <p>' . $content . '</p>
                        <p><a href="'.$post_link.'"> weiterlesen &raquo;</a></p>
                    </div>
                 </div>';
		}
		?>
    </div>
</section>

<?php get_footer( 'custom' ); ?>
