$(function() {

    // Cache the window object
    var $window = $(window);

    // Parallax background effect
    $('section[data-type="background"]').each(function() {

        var $bgobj = $(this); //assigning the object

        $(window).scroll(function() {
            // scroll the background at var speed
            // the yPos is a negative value because we're scrolling UP!

            var yPos = -($window.scrollTop() / $bgobj.data('speed'));

            // Put together our final background position
            var coords = '50% '+ yPos + 'px';

            // Move the background
            $bgobj.css({ backgroundPosition: coords });
        }); // end window scroll
    });
});

$(window).scroll(function() {
    if ($(document).scrollTop() > 500) {
        $('.navbar').addClass('shrink');
        $('.menu-image-title-hide').addClass('shrink');
        $('.navbar-nav').addClass('shrink');
    } else {
        $('.navbar').removeClass('shrink');
        $('.menu-image-title-hide').removeClass('shrink');
        $('.navbar-nav').removeClass('shrink');
    }
});