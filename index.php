<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Weltladen_Wasserburg_Theme
 */
// Advanced Custom Fields
// Aufmacher
$aufmacher_bild = get_field('aufmacher_bild', get_option('page_for_posts'));
$aufmacher_text = get_field('aufmacher_text', get_option('page_for_posts'));

get_header(); ?>

<!-- PARALLAX -->
<section id="aufmacher" class="parallax-section" data-type="background" data-speed="5"
	<?php if ( ! empty( $aufmacher_bild ) ) : ?>
        style="background: url('<?php echo $aufmacher_bild['url'] ?>') 50% 0 no-repeat;"
	<?php endif; ?>
>
    <p class="aufmacher-text"><?php echo $aufmacher_text ?></p>
</section>

<div class="container">
    <div class="row" id="primary">
        <main id="content" class="col-sm-12 news-main" role="main">
			<?php
			if ( have_posts() ) :

				if ( is_home() && ! is_front_page() ) : ?>
                    <header>
                        <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
                    </header>

					<?php
				endif;

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_format() );

				endwhile;

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>
            <div class="nav-previous alignleft"><?php next_posts_link( '&laquo;&nbsp;Ältere Beiträge' ); ?></div>
            <div class="nav-next alignright"><?php previous_posts_link( 'Neuere Beiträge&nbsp;&raquo;' ); ?></div>
        </main>
    </div>
</div>
<?php get_footer( 'custom' ); ?>
