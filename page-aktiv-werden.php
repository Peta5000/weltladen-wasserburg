<?php
/**
 * Template Name: Aktiv werden
 */
// Advanced Custom Fields
// Aufmacher
$aufmacher_bild = get_field( 'aufmacher_bild' );
$aufmacher_text = get_field( 'aufmacher_text');
// MITGLIED
$mitglied_icon            = get_field( 'mitglied_icon' );
$mitglied_ueberschrift    = get_field( 'mitglied_ueberschrift' );
$mitglied_text            = get_field( 'mitglied_text' );
$mitglied_bild            = get_field( 'mitglied_bild' );
// MITARBEIT
$mitarbeit_icon            = get_field( 'mitarbeit_icon' );
$mitarbeit_ueberschrift    = get_field( 'mitarbeit_ueberschrift' );
$mitarbeit_text            = get_field( 'mitarbeit_text' );
$mitarbeit_bild            = get_field( 'mitarbeit_bild' );
// KOOPERATION
$koop_icon            = get_field( 'koop_icon' );
$koop_ueberschrift    = get_field( 'koop_ueberschrift' );
$koop_text            = get_field( 'koop_text' );
$koop_bild            = get_field( 'koop_bild' );

get_header(); ?>

<!-- PARALLAX -->
<section id="aufmacher" class="parallax-section" data-type="background" data-speed="5"
	<?php if ( ! empty( $aufmacher_bild ) ) : ?>
        style="background: url('<?php echo $aufmacher_bild['url'] ?>') 50% 0 no-repeat;"
	<?php endif; ?>
>
    <p class="aufmacher-text"><?php echo $aufmacher_text ?></p>
</section>
<!-- WILLKOMMEN -->
<section id="willkommen">
    <div class="container">
        <h2><i class="fa <?php echo $mitglied_icon ?>"></i>&nbsp;<?php echo $mitglied_ueberschrift ?></h2>
        <div class="row">
            <div class="col-sm-6">
	            <?php echo $mitglied_text ?>
            </div>
            <div class="col-sm-6">
                <img src="<?php echo $mitglied_bild['url'] ?>" alt="<?php echo $mitglied_bild['alt'] ?>">
            </div>
        </div>
    </div>
</section>

<!-- MITARBEIT -->
<section id="mitarbeit">
    <div class="container">
        <h2><i class="fa <?php echo $mitarbeit_icon ?>"></i>&nbsp;<?php echo $mitarbeit_ueberschrift ?></h2>
        <div class="row">
            <div class="col-sm-6">
                <img src="<?php echo $mitarbeit_bild['url'] ?>" alt="<?php echo $mitarbeit_bild['alt'] ?>">
            </div>
            <div class="col-sm-6">
	            <?php echo $mitarbeit_text ?>
            </div>
        </div>
    </div>
</section>

<!-- KOOPERATION -->
<section id="kooperation">
    <div class="container">
        <h2><i class="fa <?php echo $koop_icon ?>"></i>&nbsp;<?php echo $koop_ueberschrift ?></h2>
        <div class="row">
            <div class="col-sm-6">
	            <?php echo $koop_text ?>
            </div>
            <div class="col-sm-6">
                <img src="<?php echo $koop_bild['url'] ?>" alt="<?php echo $koop_bild['alt'] ?>">
            </div>
        </div>
    </div>
</section>

<?php get_footer( 'custom' ); ?>
