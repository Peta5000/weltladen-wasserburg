<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Weltladen_Wasserburg_Theme
 */

// Advanced Custom Fields

?>

<?php wp_footer(); ?>

<!-- FOOTER -->
<footer>
    <div class="container-fluid">
        <div class="row footer-imprint-row">
            <div class="col-md-6 col-sm-12">
                <p>&copy; Weltladen Wasserburg <?php echo date("Y"); ?>. Alle Rechte vorbehalten.</p>
            </div>
            <div class="col-md-6 col-sm-12 footer-imprint-row-links">
	            <?php
	            wp_nav_menu( array(

		            'theme_location'   => 'menu-2'

	            ) );
	            ?>
            </div>
        </div>
    </div>
</footer>

<!-- BOOTSTRAP CORE JAVASCRIPT -->
<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery-3.2.1.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/main.js"></script>
<script>

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-108071962-1', 'www.weltladen-wasserburg.de');
    ga('set', 'anonymizeIp', true);
    ga('send', 'pageview');

</script>

</body>
</html>
